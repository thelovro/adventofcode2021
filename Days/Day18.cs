﻿namespace Days
{
    internal class Day18 : BaseDay
    {
        public Day18()
        {
            SampleInputPartOne.Add(@"[9,1]", "29");
            SampleInputPartOne.Add(@"[[1,2],[[3,4],5]]", "143");
            SampleInputPartOne.Add(@"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", "1384");
            SampleInputPartOne.Add(@"[[[[1,1],[2,2]],[3,3]],[4,4]]", "445");
            SampleInputPartOne.Add(@"[[[[3,0],[5,3]],[4,4]],[5,5]]", "791");
            SampleInputPartOne.Add(@"[[[[5,0],[7,4]],[5,5]],[6,6]]", "1137");
            SampleInputPartOne.Add(@"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", "3488");
            SampleInputPartOne.Add(@"[1,1]
[2,2]
[3,3]
[4,4]", "445");

            SampleInputPartOne.Add(@"[[[[4,3],4],4],[7,[[8,4],9]]]
[1,1]", "1384");

            SampleInputPartOne.Add(@"[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]", "2736");

            SampleInputPartOne.Add(@"[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]", "3488");

            SampleInputPartOne.Add(@"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", "4140");

            SampleInputPartTwo.Add(@"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", "3993");
        }

        protected override string FindFirstSolution(string[] input)
        {
            string snailFishNumber = CalculateSnailFishNumber(input);
            long solution = CalculateMagnitude(snailFishNumber);

            return solution.ToString();
        }

        private long CalculateMagnitude(string snailFishNumber)
        {
            if (!snailFishNumber.StartsWith('['))
                return Convert.ToInt64(snailFishNumber);

            string snailFishNumberTmp = snailFishNumber.Substring(1).Substring(0, snailFishNumber.Length - 2);
            int indexComma = FindCommaIndex(snailFishNumberTmp);

            long result = 3 * CalculateMagnitude(snailFishNumberTmp.Substring(0, indexComma))
                    + 2 * CalculateMagnitude(snailFishNumberTmp.Substring(indexComma + 1));

            return result;
        }

        private int FindCommaIndex(string snailFishNumberTmp)
        {
            int counter = 0;
            for (int i = 0; i < snailFishNumberTmp.Length; i++)
            {
                if (snailFishNumberTmp[i] == '[') counter++;
                if (snailFishNumberTmp[i] == ']') counter--;
                if (counter == 0 && snailFishNumberTmp[i] == ',') return i;
            }

            throw new Exception("huh??");
        }

        private string CalculateSnailFishNumber(string[] input)
        {
            string snailFishNumber = input[0];

            if (input.Length == 1) return snailFishNumber;
            
            for (int i = 1; i < input.Length; i++)
            {
                snailFishNumber = $"[{snailFishNumber},{input[i]}]";
                snailFishNumber = ReduceSnailFishNumber(snailFishNumber);
            }

            return snailFishNumber;
        }

        private string ReduceSnailFishNumber(string snailFishNumber)
        {
            string historySnailFishNumber = snailFishNumber;

            while (true)
            {
                string tmpSnailFishNumber = Explode(snailFishNumber);
                if (tmpSnailFishNumber != snailFishNumber)
                {
                    snailFishNumber = tmpSnailFishNumber;
                    continue;
                }

                snailFishNumber = Split(snailFishNumber);

                if (historySnailFishNumber != snailFishNumber)
                    historySnailFishNumber = snailFishNumber;
                else
                    break;
            }

            return snailFishNumber;
        }

        private string Explode(string snailFishNumber)
        {
            int bracketCounter = 0;
            int lastIntNumberIndex = 0;

            for (int i = 0; i < snailFishNumber.Length; i++)
            {
                if (snailFishNumber[i] == '[') bracketCounter++;
                if (snailFishNumber[i] == ']') bracketCounter--;
                if (snailFishNumber[i] != '[' && snailFishNumber[i] != ']' && snailFishNumber[i] != ',' && lastIntNumberIndex != i - 1)
                {
                    lastIntNumberIndex = i;
                }

                if (bracketCounter > 4)
                {
                    //explode!!

                    int closingBracketIndex = FindPacketClosingBracketIndex(i, snailFishNumber) + 1;
                    string subpacket = snailFishNumber.Substring(i, closingBracketIndex - i);
                    int leftNumber = Convert.ToInt32(subpacket.Substring(1).Substring(0, subpacket.Length - 2).Split(',')[0]);
                    int rightNumber = Convert.ToInt32(subpacket.Substring(1).Substring(0, subpacket.Length - 2).Split(',')[1]);

                    //replace next intiger
                    int nextIntigerIndex = FindNextIntigerIndex(i + subpacket.Length, snailFishNumber);
                    if (nextIntigerIndex != -1)
                    {
                        int tmpIndex = FindNextIntigerIndex(nextIntigerIndex + 1, snailFishNumber);
                        int num = Convert.ToInt32(snailFishNumber.Substring(nextIntigerIndex, (tmpIndex == nextIntigerIndex + 1 ? 2 : 1)).ToString());
                        snailFishNumber = snailFishNumber.Substring(0, nextIntigerIndex) + (num + rightNumber).ToString() + snailFishNumber.Substring(nextIntigerIndex + num.ToString().Length);
                    }

                    //replace with 0
                    snailFishNumber = snailFishNumber.Substring(0, i) + "0" + snailFishNumber.Substring(i + subpacket.Length);

                    //replace last intiger
                    if (lastIntNumberIndex > 0)
                    {
                        int tmpIndex = FindNextIntigerIndex(lastIntNumberIndex + 1, snailFishNumber);
                        int num = Convert.ToInt32(snailFishNumber.Substring(lastIntNumberIndex, (tmpIndex == lastIntNumberIndex + 1 ? 2 : 1)).ToString());

                        snailFishNumber = snailFishNumber.Substring(0, lastIntNumberIndex) + (num + leftNumber).ToString() + snailFishNumber.Substring(lastIntNumberIndex + 1 + (tmpIndex == lastIntNumberIndex + 1 ? 1 : 0));
                        lastIntNumberIndex = i;
                    }
                    break;
                }
            }

            return snailFishNumber;
        }

        private int FindNextIntigerIndex(int index, string snailFishNumber)
        {
            for (int i = index; i < snailFishNumber.Length; i++)
            {
                if (snailFishNumber[i] != '[' && snailFishNumber[i] != ']' && snailFishNumber[i] != ',')
                    return i;
            }
            return -1;
        }

        private int FindPacketClosingBracketIndex(int index, string snailFishNumber)
        {
            int bracketCounter = 0;

            for (int i = index; i < snailFishNumber.Length; i++)
            {
                if (snailFishNumber[i] == '[') bracketCounter++;
                if (snailFishNumber[i] == ']') bracketCounter--;

                if (bracketCounter == 0) return i;
            }
            return -1;
        }

        private string Split(string snailFishNumber)
        {
            for (int i = 1; i < snailFishNumber.Length; i++)
            {
                if (snailFishNumber[i] == '[' || snailFishNumber[i] == ']' || snailFishNumber[i] == ',' ||
                    snailFishNumber[i + 1] == '[' || snailFishNumber[i + 1] == ']' || snailFishNumber[i + 1] == ',')
                    continue;

                int number = Convert.ToInt32(snailFishNumber.Substring(i, 2));
                int division = number / 2;
                int secondNumber = division * 2 == number ? division : division + 1;

                snailFishNumber = snailFishNumber.Substring(0, i) + $"[{division},{secondNumber}]" + snailFishNumber.Substring(i + 2);
                break;
            }

            return snailFishNumber;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = 0;
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input.Length; j++)
                {
                    if (i == j) continue;

                    string snailFishNumber = CalculateSnailFishNumber(new string[] { input[i], input[j] });
                    long tmpSolution = CalculateMagnitude(snailFishNumber);
                    if (tmpSolution > solution)
                        solution = tmpSolution;
                }
            }

            return solution.ToString();
        }
    }
}