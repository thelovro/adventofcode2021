﻿namespace Days;
internal class Day02 : BaseDay
{
    public Day02()
    {
        SampleInputPartOne.Add(@"forward 5
down 5
forward 8
up 3
down 8
forward 2", "150");

        SampleInputPartTwo.Add(@"forward 5
down 5
forward 8
up 3
down 8
forward 2", "900");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int solution = Dive(input);

        return solution.ToString();
    }

    private int Dive(string[] input)
    {
        int depth = 0, forward = 0;

        foreach(string s in input)
        {
            int value = Convert.ToInt32(s.Split(" ")[1]);

            if(s.StartsWith("forward"))
            {
                forward += value;
            }
            else if (s.StartsWith("down"))
            {
                depth += value;
            }
            else if (s.StartsWith("up"))
            {
                depth -= value;
            }
        }

        return depth * forward;
    }

    protected override string FindSecondSolution(string[] input)
    {
        int solution = DiveWIthAim(input);

        return solution.ToString();
    }

    private int DiveWIthAim(string[] input)
    {
        int depth = 0;
        int forward = 0;
        int aim = 0;

        foreach (string s in input)
        {
            int value = Convert.ToInt32(s.Split(" ")[1]);

            if (s.StartsWith("forward"))
            {
                forward += value;
                depth += aim * value;
            }
            else if (s.StartsWith("down"))
            {
                aim+= value;
            }
            else if (s.StartsWith("up"))
            {
                aim -= value;
            }
        }

        return depth * forward;
    }
}