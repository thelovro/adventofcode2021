﻿namespace Days
{
    internal class Day20 : BaseDay
    {
        public Day20()
        {
            SampleInputPartOne.Add(@"..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###", "35");

            SampleInputPartTwo.Add(@"..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###", "3351");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = CalculteSolution(input, 2);

            return solution.ToString();
        }

        private int CalculteSolution(string[] input, int numberOfIterations)
        {
            string imageEnhancmentAlgorithm = input[0];
            Dictionary<(int, int), char> image = GetStartingInputImage(input);

            for (int i = 0; i < numberOfIterations; i++)
            {
                image = AddBlackPixels(image, i, imageEnhancmentAlgorithm);
                image = EnhanceImage(image, imageEnhancmentAlgorithm);
            }

            int solution = image.Where(i => i.Value == '#').Count();
            return solution;
        }

        private Dictionary<(int, int), char> EnhanceImage(Dictionary<(int, int), char> image, string imageEnhancmentAlgorithm)
        {
            Dictionary<(int, int), char> newImage = new();
            int minX = image.Min(i => i.Key.Item1);
            int maxX = image.Max(i => i.Key.Item1);
            int minY = image.Min(i => i.Key.Item2);
            int maxY = image.Max(i => i.Key.Item2);

            for (int y = minY+1; y < maxY; y++)
            {
                for (int x = minX+1; x < maxX; x++)
                {
                    string binaryTmp = image[(x - 1, y - 1)].ToString() + image[(x, y - 1)] + image[(x + 1, y - 1)] +
                                        image[(x - 1, y)] + image[(x, y)] + image[(x + 1, y)] +
                                        image[(x - 1, y + 1)] + image[(x, y + 1)] + image[(x + 1, y + 1)];

                    binaryTmp = binaryTmp.Replace('#', '1').Replace('.', '0');
                    int loc = Convert.ToInt32(binaryTmp, 2);

                    char value = imageEnhancmentAlgorithm[loc];
                    newImage.Add((x, y), value);
                }
            }
            return newImage;
        }

        private Dictionary<(int, int), char> AddBlackPixels(Dictionary<(int, int), char> image, int i, string imageEnhancmentAlgorithm)
        {
            int minX = image.Min(i => i.Key.Item1);
            int maxX = image.Max(i => i.Key.Item1);
            int minY = image.Min(i => i.Key.Item2);
            int maxY = image.Max(i => i.Key.Item2);

            for (int y = minY - 2; y <= maxY + 2; y++)
            {
                for (int x = minX - 2; x <= maxX + 2; x++)
                {
                    if (!image.ContainsKey((x, y)))
                    {
                        if(imageEnhancmentAlgorithm[0] == '#')
                        {
                            //only alternate if algorithm changes all black pixels to light!
                            if(i%2 == 0)
                                image.Add((x, y), '.'); 
                            else
                                image.Add((x, y), '#');
                        }
                        else
                            image.Add((x, y), '.');
                    }
                }
            }

            return image;
        }

        private Dictionary<(int, int), char> GetStartingInputImage(string[] input)
        {
            Dictionary<(int, int), char> image = new();
            for(int y = 2; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    image.Add((x, y - 2), input[y][x]);
                }
            }

            return image;
        }

        protected override string FindSecondSolution(string[] input)
        {
            int solution = CalculteSolution(input, 50);

            return solution.ToString();
        }
    }
}