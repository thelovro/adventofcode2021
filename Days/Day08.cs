﻿namespace Days
{
    internal class Day08 : BaseDay
    {
        public Day08()
        {
            SampleInputPartOne.Add(@"acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf", "");
            SampleInputPartOne.Add(@"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce", "26");

            SampleInputPartTwo.Add(@"acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf", "5353");
            SampleInputPartTwo.Add(@"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce", "61229");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = Calculate(input);

            return solution.ToString();
        }

        private int Calculate(string[] input)
        {
            int counter = 0;
            foreach (string s in input)
            {
                var items = s.Split('|')[1].Trim().Split(' ');
                foreach (string item in items)
                {
                    if (item.Length == 2 || item.Length == 4 || item.Length == 3 || item.Length == 7)
                        counter++;
                }
            }

            return counter;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = 0;
            foreach (string s in input)
            {
                solution += Decode(s);
            }

            return solution.ToString();
        }

        private long Decode(string row)
        {
            Digit digit = PopulateDigit(row.Split('|')[0].Trim().Split(' '));

            return GetNumber(digit, row.Split('|')[1].Trim().Split(' '));
        }

        private Digit PopulateDigit(string[] codes)
        {
            Digit digit = new();
            string codeOne = codes.First(c => c.Length == 2);
            string codeFour = codes.First(c => c.Length == 4);
            string codeSeven = codes.First(c => c.Length == 3);
            string codeEight = codes.First(c => c.Length == 7);
            string codeNine = codes.First(c => c.Length == 6 && ContainsCode(c, codeFour));
            string codeZero = codes.First(c => c.Length == 6 && c != codeNine && ContainsCode(c, codeOne));
            string codeSix = codes.First(c => c.Length == 6 && c != codeNine && c!=codeZero);
            string codeThree = codes.First(c => c.Length == 5 && c.Contains(codeOne[0]) && c.Contains(codeOne[1]));

            digit.One = codeSeven.Replace(codeOne[0].ToString(), "").Replace(codeOne[1].ToString(), "");
            digit.Seven = RemoveCodeChars(codeNine, codeFour).Replace(digit.One, "");
            digit.Six = GetIntersection(codeSix, codeOne);
            digit.Three = RemoveCodeChars (codeEight, codeSix);
            digit.Four = codeThree.Replace(digit.One, "").Replace(digit.Three, "").Replace(digit.Six, "").Replace(digit.Seven, "");

            string codeTwo = codes.First(c => c.Length == 5 && c.Contains(digit.Three) && !c.Contains(digit.Six));
            string codeFive = codes.First(c => c.Length == 5 && !c.Contains(digit.Three) && c.Contains(digit.Six));

            digit.Two = codeFive.Replace(digit.One, "").Replace(digit.Four, "").Replace(digit.Six, "").Replace(digit.Seven, "");
            digit.Five = codeTwo.Replace(digit.One, "").Replace(digit.Three, "").Replace(digit.Four, "").Replace(digit.Seven, "");

            return digit;
        }

        string GetIntersection(string codeFirst, string codeSecond)
        {
            string intersection = "";
            foreach (var c in codeSecond)
            {
                if (codeFirst.Contains(c))
                    intersection += c;
            }

            return intersection;
        }

        bool ContainsCode(string codeFirst, string codeSecond)
        {
            foreach (var c in codeSecond)
            {
                if (!codeFirst.Contains(c))
                    return false;
            }

            return true;
        }

        string RemoveCodeChars(string codeFirst, string codeSecond)
        {
            foreach(var c in codeSecond)
            {
                if (codeFirst.Contains(c))
                    codeFirst = codeFirst.Replace(c.ToString(), "");
            }

            return codeFirst;
        }

        class Digit
        {
            public string One { get; set; }
            public string Two { get; set; }
            public string Three { get; set; }
            public string Four { get; set; }
            public string Five { get; set; }
            public string Six { get; set; }
            public string Seven { get; set; }
        }

        int GetNumber(Digit digit, string[] codes)
        {
            int number = 0;
            number += 1000 * GetDigit(digit, codes[0]);
            number += 100 * GetDigit(digit, codes[1]);
            number += 10 * GetDigit(digit, codes[2]);
            number += 1 * GetDigit(digit, codes[3]);

            return number;
        }
        int GetDigit(Digit digit, string code)
        {
            if (code.Length == 7) return 8;
            if (code.Length == 6 && !code.Contains(digit.Four)) return 0;
            if (code.Length == 6 && !code.Contains(digit.Three)) return 6;
            if (code.Length == 6 && !code.Contains(digit.Five)) return 9;
            if (code.Length == 5 && !code.Contains(digit.Three) && !code.Contains(digit.Five)) return 5;
            if (code.Length == 5 && !code.Contains(digit.Two) && !code.Contains(digit.Six)) return 2;
            if (code.Length == 5 && !code.Contains(digit.Two) && !code.Contains(digit.Five)) return 3;
            if (code.Length == 4) return 4;
            if (code.Length == 3) return 7;
            if (code.Length == 2) return 1;

            throw new Exception("AAAAAAAAA");
        }
    }
}