﻿namespace Days
{
    internal class Day24 : BaseDay
    {
        public Day24()
        {
            SampleInputPartOne.Add(@"", "There is no test");

            SampleInputPartTwo.Add(@"", "There is no test");
        }

        protected override string FindFirstSolution(string[] input)
        {
            if (IsTest) return "There is no test";

            List<string> validNumbers = Calculate(0, 0);
            string solution = validNumbers.Max();
            return solution;
        }

        Dictionary<(int groupNum, long prevZ), List<string>> visited = new();
        private List<string> Calculate(int groupId, long z)
        {
            if(visited.ContainsKey((groupId, z)))
                return visited[(groupId, z)].ToList();

            List<string> solutionsSoFar = new();
            if (groupId > 13)
            {
                if (z == 0) return new() { string.Empty };
                return null;
            }

            //since we divide Z by numbers in divZ, we can skip numbers that are too big
            //without this, code takes about 90s to run
            long sum = 1;
            for (int i = groupId; i < 14; i++) sum *= divZ[i];
            if(z > sum) return null;

            List<string> nextGroupSolutions;
            foreach (int i in Enumerable.Range(1, 9))
            {
                long nextZ = SolveOperationsGroup(groupId, z, i);

                nextGroupSolutions = Calculate(groupId + 1, nextZ);
                if(nextGroupSolutions != null)
                {
                    foreach (var s in nextGroupSolutions)
                    {
                        solutionsSoFar.Add($"{i}{s}");
                    }
                }
            }
            visited[(groupId, z)] = solutionsSoFar;
            
            return solutionsSoFar;
        }

        //parse different digits at three operations manually
        List<int> divZ = new() { 1, 1, 1, 26, 1, 26, 1, 1, 1, 26, 26, 26, 26, 26 };
        List<int> addX = new() { 10, 13, 13, -11, 11, -4, 12, 12, 15, -2, -5, -11, -13, -10 };
        List<int> addY = new() { 13, 10, 3, 1, 9, 3, 5, 1, 0, 13, 7, 15, 12, 8 };
        private long SolveOperationsGroup(int groupId, long z, int w)
        { 
            long x = z % 26 + addX[groupId];
            z = z / divZ[groupId];

            if (x != w)
            {
                z = (26 * z) + w + addY[groupId];
            }

            return z;
        }

        protected override string FindSecondSolution(string[] input)
        {
            if (IsTest) return "There is no test";

            List<long> MaxZAtStep = new();
            for (int i = 0; i < divZ.Count; i++)
            {
                MaxZAtStep.Add(divZ.Skip(i).Aggregate(1L, (a, b) => a * b));
            }

            List<string> validNumbers = Calculate(0, 0);
            string solution = validNumbers.Min();
            return solution;
        }
    }
}