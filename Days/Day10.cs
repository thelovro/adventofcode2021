﻿namespace Days
{
    internal class Day10 : BaseDay
    {
        public Day10()
        {
            SampleInputPartOne.Add(@"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]", "26397");

            SampleInputPartTwo.Add(@"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]", "288957");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = CalculatePenaltyForCorruptedLines(input);

            return solution.ToString();
        }

        private int CalculatePenaltyForCorruptedLines(string[] input)
        {
            int penalty = 0;

            foreach(string row in input)
            {
                Stack<char> brackets = new();

                foreach(char col in row)
                {
                    if(col == '(' ||col =='[' ||col=='{' ||col=='<')
                    {
                        brackets.Push(col);
                    }
                    else
                    {
                        char openingBracket = brackets.Pop();
                        if (openingBracket == '(' && col != ')'
                        || openingBracket == '[' && col != ']'
                        || openingBracket == '{' && col != '}'
                        || openingBracket == '<' && col != '>')
                        {
                            penalty += GetPenalty(col);
                            break;
                        }
                    }
                }
            }

            return penalty;
        }

        private int GetPenalty(char col)
        {
            if (col == ')') return 3;
            if (col == ']') return 57;
            if (col == '}') return 1197;
            if (col == '>') return 25137;

            throw new Exception("AAAAAAAAAAAA");
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = CalculatePenaltyForIncompleteLines(input);

            return solution.ToString();
        }

        private long CalculatePenaltyForIncompleteLines(string[] input)
        {
            List<long> penalties = new();

            foreach (string row in input)
            {
                Stack<char> brackets = new();
                bool isCorrupted = false;
                
                foreach (char col in row)
                {
                    if (col == '(' || col == '[' || col == '{' || col == '<')
                    {
                        brackets.Push(col);
                    }
                    else
                    {
                        char openingBracket = brackets.Pop();
                        if (openingBracket == '(' && col != ')'
                        || openingBracket == '[' && col != ']'
                        || openingBracket == '{' && col != '}'
                        || openingBracket == '<' && col != '>')
                        {
                            //discard corrupted lines
                            isCorrupted = true;
                            break;
                        }
                    }
                }

                if (isCorrupted) continue;

                penalties.Add(CalculateAutoCompletePenalty(string.Join("", brackets.ToList())));
            }

            penalties.Sort();
            long penalty = penalties[penalties.Count / 2];

            return penalty;
        }

        private long CalculateAutoCompletePenalty(string added)
        {
            long penalty = 0;
            foreach(char col in added)
            {
                penalty *= 5;

                if (col == '(') penalty += 1;
                if (col == '[') penalty += 2;
                if (col == '{') penalty += 3;
                if (col == '<') penalty += 4;
            }

            return penalty;
        }
    }
}