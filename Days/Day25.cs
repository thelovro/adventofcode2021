﻿namespace Days
{
    internal class Day25 : BaseDay
    {
        public Day25()
        {
            SampleInputPartOne.Add(@"v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>", "58");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            (List<(int, int)> east, List<(int, int)> south, int maxX, int maxY) = GetCucumbersInitialLocations(input);

            long steps = MoveCucumbers(east, south, maxX, maxY);

            return steps.ToString();
        }

        private long MoveCucumbers(List<(int, int)> east, List<(int, int)> south, int maxX, int maxY)
        {
            long steps = 0;
            bool isChanged = true;
            while(isChanged)
            {
                isChanged = false;

                List<(int, int)> newEast = new();
                for (int i = 0; i < east.Count; i++)
                {
                    (int, int) e = east[i];
                    var locationRight = (e.Item1 == maxX ? 0 : e.Item1 + 1, e.Item2);
                    if (!east.Contains(locationRight) && !south.Contains(locationRight))
                    {
                        isChanged = true;
                        e.Item1 = locationRight.Item1;
                    }
                    newEast.Add(e);
                }
                east = newEast.OrderBy(e => e.Item2).ThenBy(e => e.Item1).ToList();

                List<(int, int)> newSouth = new();
                for (int i = 0; i < south.Count; i++)
                {
                    (int, int) s = south[i];
                    var locationDown = (s.Item1, s.Item2 == maxY ? 0 : s.Item2 + 1);
                    if (!east.Contains(locationDown) && !south.Contains(locationDown))
                    {
                        isChanged = true;
                        s.Item2 = locationDown.Item2;
                    }
                    newSouth.Add(s);
                }
                south = newSouth.OrderBy(s => s.Item2).ThenBy(s => s.Item1).ToList();

                if (isChanged) steps++;
            }

            return steps + 1;
        }

        private (List<(int, int)>, List<(int, int)>, int maxX, int maxY) GetCucumbersInitialLocations(string[] input)
        {
            List<(int, int)> east = new();
            List<(int, int)> south = new();

            for (int y = 0; y < input.Length; y++)
            {
                string line = input[y];
                for (int x = 0; x < line.Length; x++)
                {
                    char c = line[x];
                    if (c == '.') continue;
                    if (c == '>') east.Add((x, y));
                    if (c == 'v') south.Add((x, y));
                }
            }

            return (east, south, input[0].Length - 1, input.Length - 1);
        }

        protected override string FindSecondSolution(string[] input)
        {
            return "There is no solution for second part ;)";
        }
    }
}