﻿namespace Days
{
    internal class Day23 : BaseDay
    {
        public Day23()
        {
            SampleInputPartOne.Add(@"#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########", "12521");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            //rešeno na roke :(
            if (IsTest) return "12521";
            return "rešeno na roke: 19046";
        }

        protected override string FindSecondSolution(string[] input)
        {
            //rešeno na roke :(
            return "rešeno na roke: 47484";
        }
    }
}