﻿using System.Text.RegularExpressions;

namespace Days
{
    internal class Day19 : BaseDay
    {
        public Day19()
        {
            SampleInputPartOne.Add(@"--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14
", "79");

            SampleInputPartTwo.Add(@"--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14
", "3621");

        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, Scanner> scanners = GetScanners(input);
            scanners = FindMatches(scanners);
            List<(int, int, int)> beacons = FindUnionBeacons(scanners);

            return beacons.Count.ToString();
        }

        private List<(int, int, int)> FindUnionBeacons(Dictionary<int, Scanner> scanners)
        {
            List<(int, int, int)> beacons = new();
            foreach (var scanner in scanners)
            {
                beacons = beacons.Union(scanner.Value.GetCoordinatesRelativeToScannerZeroTemp()).ToList();
            }
            return beacons;
        }

        private Dictionary<int, Scanner> FindMatches(Dictionary<int, Scanner> scanners)
        {
            while (scanners.Where(s => s.Key > 0).Any(s => s.Value.ScannerCoordinates == (0, 0, 0)))
            {
                foreach (var firstScanner in scanners.Where(s => s.Key == 0 || s.Value.ScannerCoordinates != (0, 0, 0)))
                {
                    foreach (var secondScanner in scanners.Where(s => s.Key > 0 && s.Value.ScannerCoordinates == (0, 0, 0)))
                    {
                        bool matchFound = FindMatch(firstScanner, secondScanner);
                    }
                }
            }

            return scanners;
        }

        private bool FindMatch(KeyValuePair<int, Scanner> firstScanner, KeyValuePair<int, Scanner> secondScanner)
        {
            for (int b1 = 0; b1 < firstScanner.Value.Beacons.Count; b1++)
            {
                var s1Beacons = firstScanner.Value.GetRelativeCoordinatesTransformedToScannerZero(b1);

                for (int b2 = 0; b2 < secondScanner.Value.Beacons.Count; b2++)
                {
                    for (int j = 0; j <= 5; j++)
                    {
                        var s2Beacons = secondScanner.Value.GetRelativeCoordinates(b2)[j];
                        foreach (var s1BeaconsPermutation in GetDirectionPermutations(s2Beacons))
                        {
                            var intersect = s1Beacons.Intersect(s1BeaconsPermutation.Value).ToList();
                            if (intersect.Count < 12) continue;

                            secondScanner.Value.ScannerDirectionPermutationId = s1BeaconsPermutation.Key;
                            secondScanner.Value.ScannerRotationPermutationId = j;

                            var s1relative = s1Beacons.IndexOf((0, 0, 0));
                            var s2relative = s2Beacons.IndexOf((0, 0, 0));

                            var beacon1 = firstScanner.Value.Beacons[s1relative];
                            var beacon2 = secondScanner.Value.Beacons[s2relative];

                            beacon1 = firstScanner.Value.GetCoordinatesRelativeToScannerZeroTemp()[s1relative];
                            beacon2 = secondScanner.Value.GetCoordinatesRelativeToScannerZeroTemp()[s2relative];

                            secondScanner.Value.ScannerCoordinates = (beacon1.Item1 - beacon2.Item1, beacon1.Item2 - beacon2.Item2, beacon1.Item3 - beacon2.Item3);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private Dictionary<int, List<(int, int, int)>> GetDirectionPermutations(List<(int, int, int)> beacons)
        {
            Dictionary<int, List<(int, int, int)>> permutations = new();
            permutations.Add(0, beacons.Select(b => (b.Item1 * 1, b.Item2 * 1, b.Item3 * 1)).ToList());
            permutations.Add(1, beacons.Select(b => (b.Item1 * 1, b.Item2 * 1, b.Item3 * -1)).ToList());
            permutations.Add(2, beacons.Select(b => (b.Item1 * 1, b.Item2 * -1, b.Item3 * 1)).ToList());
            permutations.Add(3, beacons.Select(b => (b.Item1 * 1, b.Item2 * -1, b.Item3 * -1)).ToList());
            permutations.Add(4, beacons.Select(b => (b.Item1 * -1, b.Item2 * 1, b.Item3 * 1)).ToList());
            permutations.Add(5, beacons.Select(b => (b.Item1 * -1, b.Item2 * 1, b.Item3 * -1)).ToList());
            permutations.Add(6, beacons.Select(b => (b.Item1 * -1, b.Item2 * -1, b.Item3 * 1)).ToList());
            permutations.Add(7, beacons.Select(b => (b.Item1 * -1, b.Item2 * -1, b.Item3 * -1)).ToList());

            return permutations;
        }
        private Dictionary<int, Scanner> GetScanners(string[] input)
        {
            Dictionary<int, Scanner> scanners = new();
            Scanner scanner = new();
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].StartsWith("---"))
                {
                    Regex r = new Regex("--- scanner (?<scannerId>[0-9]+)+ ---");
                    int id = Convert.ToInt32(r.Match(input[i]).Groups["scannerId"].Value);
                    scanner = new Scanner() { ScannerId = id };
                }
                else if (input[i].Trim().Length == 0)
                {
                    scanners.Add(scanner.ScannerId, scanner);
                }
                else
                    scanner.Beacons.Add((Convert.ToInt32(input[i].Split(',')[0]),
                                        Convert.ToInt32(input[i].Split(',')[1]),
                                        Convert.ToInt32(input[i].Split(',')[2])));
            }

            return scanners;
        }

        private class Scanner
        {
            public int ScannerId { get; set; }

            public (int, int, int) ScannerCoordinates { get; set; }
            public int ScannerDirectionPermutationId { get; set; }
            public int ScannerRotationPermutationId { get; set; }

            private List<(int, int, int)> _beacons = new();
            public List<(int, int, int)> Beacons { get { return _beacons; } }

            public Dictionary<int, List<(int, int, int)>> GetRelativeCoordinates(int beaconId)
            {
                (int, int, int) baseBeacon = Beacons[beaconId];

                List<(int, int, int)> relativeDistances1 = new();
                List<(int, int, int)> relativeDistances2 = new();
                List<(int, int, int)> relativeDistances3 = new();
                List<(int, int, int)> relativeDistances4 = new();
                List<(int, int, int)> relativeDistances5 = new();
                List<(int, int, int)> relativeDistances6 = new();

                for (int i = 0; i < Beacons.Count; i++)
                {
                    (int, int, int) beacon = Beacons[i];

                    int x = beacon.Item1 - baseBeacon.Item1;
                    int y = beacon.Item2 - baseBeacon.Item2;
                    int z = beacon.Item3 - baseBeacon.Item3;

                    relativeDistances1.Add((x, y, z));
                    relativeDistances2.Add((x, z, y));
                    relativeDistances3.Add((y, x, z));
                    relativeDistances4.Add((y, z, x));
                    relativeDistances5.Add((z, x, y));
                    relativeDistances6.Add((z, y, x));
                }

                Dictionary<int, List<(int, int, int)>> relativeCoordinates = new();
                relativeCoordinates.Add(0, relativeDistances1);
                relativeCoordinates.Add(1, relativeDistances2);
                relativeCoordinates.Add(2, relativeDistances3);
                relativeCoordinates.Add(3, relativeDistances4);
                relativeCoordinates.Add(4, relativeDistances5);
                relativeCoordinates.Add(5, relativeDistances6);

                return relativeCoordinates;
            }

            private (int, int, int) GetValueBasedOnDirectionPermutation((int, int, int) beacon, int permutation)
            {
                (int, int, int) value = permutation switch
                {
                    0 => (beacon.Item1 * 1, beacon.Item2 * 1, beacon.Item3 * 1),
                    1 => (beacon.Item1 * 1, beacon.Item2 * 1, beacon.Item3 * -1),
                    2 => (beacon.Item1 * 1, beacon.Item2 * -1, beacon.Item3 * 1),
                    3 => (beacon.Item1 * 1, beacon.Item2 * -1, beacon.Item3 * -1),
                    4 => (beacon.Item1 * -1, beacon.Item2 * 1, beacon.Item3 * 1),
                    5 => (beacon.Item1 * -1, beacon.Item2 * 1, beacon.Item3 * -1),
                    6 => (beacon.Item1 * -1, beacon.Item2 * -1, beacon.Item3 * 1),
                    7 => (beacon.Item1 * -1, beacon.Item2 * -1, beacon.Item3 * -1)
                };

                return value;
            }

            private (int, int, int) GetValueBasedOnRotationPermutation((int, int, int) beacon, int permutation)
            {
                (int, int, int) value = permutation switch
                {
                    0 => (beacon.Item1, beacon.Item2, beacon.Item3),
                    1 => (beacon.Item1, beacon.Item3, beacon.Item2),
                    2 => (beacon.Item2, beacon.Item1, beacon.Item3),
                    3 => (beacon.Item2, beacon.Item3, beacon.Item1),
                    4 => (beacon.Item3, beacon.Item1, beacon.Item2),
                    5 => (beacon.Item3, beacon.Item2, beacon.Item1),
                };

                return value;
            }

            public List<(int, int, int)> GetRelativeCoordinatesTransformedToScannerZero(int baseBeaconId)
            {
                List<(int, int, int)> relativeDistances = new();
                (int, int, int) baseBeacon = Beacons[baseBeaconId];

                for (int i = 0; i < Beacons.Count; i++)
                {
                    (int, int, int) beacon = Beacons[i];

                    int x = beacon.Item1 - baseBeacon.Item1;
                    int y = beacon.Item2 - baseBeacon.Item2;
                    int z = beacon.Item3 - baseBeacon.Item3;

                    (x, y, z) = GetValueBasedOnRotationPermutation((x, y, z), ScannerRotationPermutationId);
                    (x, y, z) = GetValueBasedOnDirectionPermutation((x, y, z), ScannerDirectionPermutationId);

                    relativeDistances.Add((x, y, z));
                }

                return relativeDistances;
            }

            public List<(int, int, int)> GetCoordinatesRelativeToScannerZeroTemp()
            {
                List<(int, int, int)> relativeDistances0 = new();

                for (int i = 0; i < Beacons.Count; i++)
                {
                    (int, int, int) beacon = Beacons[i];
                    beacon = GetValueBasedOnRotationPermutation(beacon, ScannerRotationPermutationId);
                    beacon = GetValueBasedOnDirectionPermutation(beacon, ScannerDirectionPermutationId);

                    int x = beacon.Item1 + ScannerCoordinates.Item1;
                    int y = beacon.Item2 + ScannerCoordinates.Item2;
                    int z = beacon.Item3 + ScannerCoordinates.Item3;

                    relativeDistances0.Add((x, y, z));
                }

                return relativeDistances0;
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, Scanner> scanners = GetScanners(input);
            scanners = FindMatches(scanners);

            Dictionary<(int, int), int> manhattans = FindManhattanDistances(scanners);

            return manhattans.Select(x => x.Value).Max().ToString();
        }

        private Dictionary<(int, int), int> FindManhattanDistances(Dictionary<int, Scanner> scanners)
        {
            Dictionary<(int, int), int> manhattans = new();
            foreach (var firstScanner in scanners)
            {
                foreach (var secondScanner in scanners.Where(s => s.Key > firstScanner.Key))
                {
                    manhattans.Add((firstScanner.Key, secondScanner.Key), Manhattan(firstScanner.Value.ScannerCoordinates, secondScanner.Value.ScannerCoordinates));
                }
            }

            return manhattans;
        }

        private int Manhattan((int, int, int) s1, (int, int, int) s2)
        {
            return Math.Abs(s1.Item1 - s2.Item1) + Math.Abs(s1.Item2 - s2.Item2) + Math.Abs(s1.Item3 - s2.Item3);
        }
    }
}