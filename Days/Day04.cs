﻿namespace Days;

internal class Day04 : BaseDay
{
    public Day04()
    {
        SampleInputPartOne.Add(@"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7", "4512");

        SampleInputPartTwo.Add(@"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7", "1924");
    }

    protected override string FindFirstSolution(string[] input)
    {
        List<int> numbers;
        List<Dictionary<int, bool>> boards;

        (numbers, boards) = ParseInput(input);

        int solution = PlayBingo(numbers, boards);

        return solution.ToString();
    }

    private int PlayBingo(List<int> numbers, List<Dictionary<int,bool>> boards)
    {
        foreach(int number in numbers)
        {
            foreach(var board in boards)
            {
                if (!board.ContainsKey(number)) continue;
                
                board[number] = true;

                if (CheckForBingo(board))
                {
                    //we have a winner
                    return board.Where(x => !x.Value).Sum(x => x.Key) * number;
                }
            }
        }

        return -1;
    }

    private bool CheckForBingo(Dictionary<int, bool> board)
    {
        //check rows
        for (int i = 0; i < 5; i++)
        {
            if (board.ElementAt(i * 5).Value && board.ElementAt(i * 5 + 1).Value && board.ElementAt(i * 5 + 2).Value && board.ElementAt(i * 5 + 3).Value && board.ElementAt(i * 5 + 4).Value)
                return true;
        }

        //check columns
        for (int i = 0; i < 5; i++)
        {
            if (board.ElementAt(i).Value && board.ElementAt(i + 5).Value && board.ElementAt(i + 10).Value && board.ElementAt(i + 15).Value && board.ElementAt(20).Value)
                return true;
        }

        return false;
    }

    private (List<int> numbers, List<Dictionary<int, bool>> boards) ParseInput(string[] input)
    {
        List<int> numbers = input[0].Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
        List<Dictionary<int, bool>> boards = new();

        for (int i = 2; i< input.Length; i++)
        {
            Dictionary<int, bool> board= new();

            for (int j = 0; j < 5; j++)
            {
                string row = input[i+j].TrimStart().Replace("  ", " ");
                foreach (var item in row.Split(' '))
                {
                    board.Add(Convert.ToInt32(item), false);
                }
            }

            boards.Add(board);

            i += 5;
        }
      

        return (numbers, boards);
    }

    protected override string FindSecondSolution(string[] input)
    {
        List<int> numbers;
        List<Dictionary<int, bool>> boards;

        (numbers, boards) = ParseInput(input);

        int solution = PlayBingoLastBoard(numbers, boards);

        return solution.ToString();
    }

    private int PlayBingoLastBoard(List<int> numbers, List<Dictionary<int, bool>> boards)
    {
        List<int> winningBoardIndexes = new();

        foreach (int number in numbers)
        {
            foreach (var board in boards)
            {
                if (winningBoardIndexes.Contains(boards.IndexOf(board))) continue;
                if (!board.ContainsKey(number)) continue;

                board[number] = true;

                if (CheckForBingo(board))
                {
                    //we have a winner
                    winningBoardIndexes.Add(boards.IndexOf(board));
                }

                if(winningBoardIndexes.Count == boards.Count)
                {
                    //last winning board
                    return board.Where(x => !x.Value).Sum(x => x.Key) * number;
                }
            }
        }

        return -1;
    }
}