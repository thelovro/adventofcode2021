﻿namespace Days
{
    internal class Day11 : BaseDay
    {
        public Day11()
        {
            SampleInputPartOne.Add(@"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526", "1656");

            SampleInputPartTwo.Add(@"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526", "195");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int, int), int> octopuses = ParseOctopuses(input);

            int sumFlashes = 0;
            for (int i = 0; i < 100; i++)
            {
                //increase energyLevel
                foreach (var octopus in octopuses)
                {
                    octopuses[octopus.Key]++;
                }

                //flash!
                int flashes = Flash(octopuses);
                sumFlashes += flashes;
            }

            return sumFlashes.ToString();
        }

        private static Dictionary<(int, int), int> ParseOctopuses(string[] input)
        {
            Dictionary<(int, int), int> octopuses = new();
            for (int y = 0; y < input.Length; y++)
            {
                string s = input[y];
                for (int x = 0; x < s.Length; x++)
                {
                    octopuses.Add((x, y), Convert.ToInt32(s[x].ToString()));
                }
            }

            return octopuses;
        }

        private int Flash(Dictionary<(int, int), int> octopuses)
        {
            Queue<(int, int)> flashFocations = new();
            int flashes = 0;

            foreach (var o in octopuses.Where(o => o.Value > 9))
            {
                flashFocations.Enqueue(o.Key);
            }

            while (flashFocations.Count > 0)
            {
                var o = flashFocations.Dequeue();
                if (octopuses[o] == 0) continue;

                flashes++;
                octopuses[o] = 0;

                //increase energy level on adjacent octopuses
                for (int y = -1; y <= 1; y++)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        var loc = (o.Item1 + x, o.Item2 + y);
                        if (loc == o) continue;
                        if (!octopuses.ContainsKey(loc)) continue;
                        if (octopuses[loc] == 0) continue;

                        octopuses[loc]++;

                        if (octopuses[loc] > 9)
                            flashFocations.Enqueue(loc);
                    }
                }
            }

            return flashes;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(int, int), int> octopuses = ParseOctopuses(input);

            int stepsToFlashAllAtOnce = 0;
            for (int i = 0; i < 1000; i++)
            {
                //increase energyLevel
                foreach (var octopus in octopuses)
                {
                    octopuses[octopus.Key]++;
                }

                //flash!
                int flashes = Flash(octopuses);
                if (flashes == 100)
                {
                    stepsToFlashAllAtOnce = i + 1;
                    break;
                }
            }

            return stepsToFlashAllAtOnce.ToString();
        }
    }
}