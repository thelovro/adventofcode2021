﻿namespace Days
{
    internal class Day05 : BaseDay
    {
        public Day05()
        {
            SampleInputPartOne.Add(@"0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2", "5");

            SampleInputPartTwo.Add(@"0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2", "12");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = FindSegments(input, true);

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int solution = FindSegments(input, false);

            return solution.ToString();
        }

        private int FindSegments(string[] input, bool onlyOnSameAxis)
        {
            Dictionary<(int, int), int> segments = new();
            foreach (string part in input)
            {
                (int, int) startPoint = (Convert.ToInt32(part.Split(" -> ")[0].Split(',')[0]), Convert.ToInt32(part.Split(" -> ")[0].Split(',')[1]));
                (int, int) endPoint = (Convert.ToInt32(part.Split(" -> ")[1].Split(',')[0]), Convert.ToInt32(part.Split(" -> ")[1].Split(',')[1]));

                //only check the ones with the same x or y axis
                if (onlyOnSameAxis && startPoint.Item1 != endPoint.Item1 && startPoint.Item2 != endPoint.Item2) continue;

                while (true)
                {
                    if (segments.ContainsKey(startPoint))
                    {
                        segments[startPoint]++;
                    }
                    else
                    {
                        segments.Add(startPoint, 1);
                    }

                    if (startPoint.Item1 == endPoint.Item1 && startPoint.Item2 == endPoint.Item2)
                        break;

                    if (startPoint.Item1 != endPoint.Item1)
                        startPoint.Item1 = startPoint.Item1 < endPoint.Item1 ? startPoint.Item1 + 1 : startPoint.Item1 - 1;

                    if (startPoint.Item2 != endPoint.Item2)
                        startPoint.Item2 = startPoint.Item2 < endPoint.Item2 ? startPoint.Item2 + 1 : startPoint.Item2 - 1;
                }
            }

            return segments.Where(x => x.Value > 1).Count();
        }
    }
}