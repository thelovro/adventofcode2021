﻿namespace Days
{
    internal class Day14 : BaseDay
    {
        public Day14()
        {
            SampleInputPartOne.Add(@"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C", "1588");

            SampleInputPartTwo.Add(@"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C", "2188189693529");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long solution = FindSolution(input, 10);
            return solution.ToString();
        }

        private long FindSolution(string[] input, int numberOfSteps)
        {
            string polymerTemplate = input[0];
            Dictionary<string, string> rules = GetPairs(input);
            Dictionary<string, long> options = ApplySteps(rules, polymerTemplate, numberOfSteps);
            Dictionary<string, long> quantites = GetQuantities(options, polymerTemplate);

            long solution = quantites.Max(c => c.Value) - quantites.Min(c => c.Value);
            return solution;
        }

        private Dictionary<string, long> GetQuantities(Dictionary<string, long> options, string polymerTemplate)
        {
            Dictionary<string, long> quantites = new();
            foreach (var pair in options)
            {
                if(quantites.ContainsKey(pair.Key[0].ToString()))
                    quantites[pair.Key[0].ToString()] += pair.Value;
                else
                    quantites.Add(pair.Key[0].ToString(), pair.Value);
            }

            //add last letter to counter
            quantites[polymerTemplate[polymerTemplate.Length - 1].ToString()]++;

            return quantites;
        }

        private Dictionary<string, long> ApplySteps(Dictionary<string, string> rules, string polymerTemplate, int numberOfSteps)
        {
            Dictionary<string, long> options = GetEmptyDictionary(rules);
            //fill in the init data
            for (int x = 1; x < polymerTemplate.Length; x++)
            {
                string key = (polymerTemplate[x - 1].ToString() + polymerTemplate[x].ToString());
                options[key]++;
            }

            //do the magic
            for (int i = 0; i < numberOfSteps; i++)
            {
                Dictionary<string, long> newOptions = GetEmptyDictionary(rules);
                foreach (var option in options)
                {
                    string itemOne = option.Key[0].ToString() + rules[option.Key];
                    string itemTwo = rules[option.Key] + option.Key[1].ToString();

                    newOptions[itemOne] += option.Value;
                    newOptions[itemTwo] += option.Value;
                }

                options = newOptions;
            }

            return options;
        }

        private Dictionary<string, string> GetPairs(string[] input)
        {
            Dictionary<string, string> rules = new();
            for(int i=2;i<input.Length;i++)
            {
                rules.Add(input[i].Split(" -> ")[0], input[i].Split(" -> ")[1]);
            }

            return rules;
        }

        private Dictionary<string, long> GetEmptyDictionary(Dictionary<string, string> pairs)
        {
            Dictionary<string, long> options = new();
            foreach(KeyValuePair<string, string> pair in pairs)
            {
                options.Add(pair.Key, 0);
            }

            return options;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = FindSolution(input, 40);
            return solution.ToString();
        }
    }
}