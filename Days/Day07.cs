﻿namespace Days
{
    internal class Day07 : BaseDay
    {
        public Day07()
        {
            SampleInputPartOne.Add(@"16,1,2,0,4,2,7,1,2,14", "37");

            SampleInputPartTwo.Add(@"16,1,2,0,4,2,7,1,2,14", "168");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long solution = CalculateFuelNeeded(input, false);

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = CalculateFuelNeeded(input, true);

            return solution.ToString();
        }

        private long CalculateFuelNeeded(string[] input, bool isFakeFactorial)
        {
            Dictionary<int, int> crabs = new();
            foreach (var part in input[0].Split(','))
            {
                int currentPosition = Convert.ToInt32(part);
                if (crabs.ContainsKey(currentPosition))
                {
                    crabs[currentPosition]++;
                }
                else
                {
                    crabs.Add(currentPosition, 1);
                }
            }

            long fuelNeeded = long.MaxValue;
            long possiblePositons = input[0].Split(',').Length;
            for (int i = 0; i < possiblePositons; i++)
            {
                long tmpFuelNeeded = 0;
                foreach (var item in crabs)
                {
                    if(isFakeFactorial)
                        tmpFuelNeeded += FakeFactorial(Math.Abs(item.Key - i)) * item.Value;
                    else
                        tmpFuelNeeded += Math.Abs(item.Key - i) * item.Value;
                }
                if (tmpFuelNeeded < fuelNeeded)
                    fuelNeeded = tmpFuelNeeded;
            }

            return fuelNeeded;
        }

        private int FakeFactorial(int f)
        {
            if (f == 0)
                return 0;
            else
                return f + FakeFactorial(f - 1);
        }
    }
}