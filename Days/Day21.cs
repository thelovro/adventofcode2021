﻿namespace Days
{
    internal class Day21 : BaseDay
    {
        public Day21()
        {
            SampleInputPartOne.Add(@"Player 1 starting position: 4
Player 2 starting position: 8", "739785");

            SampleInputPartTwo.Add(@"Player 1 starting position: 4
Player 2 starting position: 8", "444356092776315");
        }

        protected override string FindFirstSolution(string[] input)
        {
            (int playerOnePosition, int playerTwoPosition) = GetPlayerPositions(input);
            int playerOneScore = 0;
            int playerTwoScore = 0;
            int dice = 1;
            bool isPlayerOneTurn = true;

            while (playerOneScore < 1000 && playerTwoScore < 1000)
            {
                int playerDice = dice + dice + 1 + dice + 2;
                dice += 3;
                
                if (isPlayerOneTurn)
                {
                    playerOnePosition += playerDice;
                    while (playerOnePosition > 10) playerOnePosition -= 10;
                    playerOneScore += playerOnePosition;
                }
                else
                {
                    playerTwoPosition += playerDice;
                    while (playerTwoPosition > 10) playerTwoPosition -= 10;
                    playerTwoScore += playerTwoPosition;
                }

                isPlayerOneTurn = !isPlayerOneTurn;
            }

            int losingPlayerScore = Math.Min(playerOneScore, playerTwoScore);

            int solution = losingPlayerScore * (dice - 1);
            return solution.ToString();
        }

        private (int playerOnePosition, int playerTwoPosition) GetPlayerPositions(string[] input)
        {
            int playerOnePosition = Convert.ToInt32(input[0].Split(':')[1].TrimStart());
            int playerTwoPosition = Convert.ToInt32(input[1].Split(':')[1].TrimStart());

            return (playerOnePosition, playerTwoPosition);
        }

        protected override string FindSecondSolution(string[] input)
        {
            long playerOneWins = 0;
            long playerTwoWins = 0;
            (int playerOnePositionInit, int playerTwoPositionInit) = GetPlayerPositions(input);

            Queue<((int, int), (int, int), long, bool)> queue = new();
            queue.Enqueue(((playerOnePositionInit, 0), (playerTwoPositionInit, 0), 1, true));
            var diceValues = GetDistinctDiceValues();

            while (queue.Count > 0)
            {
                ((int, int) playerOne, (int, int) playerTwo, long gamesMultiplicationInit, bool isPlayerOneTurn) = queue.Dequeue();

                foreach (var dice in diceValues)
                {
                    long gamesMultiplication = gamesMultiplicationInit * dice.Value;

                    if (isPlayerOneTurn)
                    {
                        int playerPosition = playerOne.Item1 + dice.Key;
                        while (playerPosition > 10) playerPosition -= 10;

                        int playerScore = playerOne.Item2 + playerPosition;

                        if (playerScore >= 21)
                        {
                            playerOneWins += gamesMultiplication;
                        }
                        else
                        {
                            queue.Enqueue(((playerPosition, playerScore), (playerTwo.Item1, playerTwo.Item2), gamesMultiplication, !isPlayerOneTurn));
                        }
                    }
                    else
                    {
                        int playerPosition = playerTwo.Item1 + dice.Key;
                        while (playerPosition > 10) playerPosition -= 10;

                        int playerScore = playerTwo.Item2 + playerPosition;

                        if (playerScore >= 21)
                        {
                            playerTwoWins += gamesMultiplication;
                        }
                        else
                        {
                            queue.Enqueue(((playerOne.Item1, playerOne.Item2), (playerPosition, playerScore), gamesMultiplication, !isPlayerOneTurn));
                        }
                    }
                }
            }

            long solution = Math.Max(playerOneWins, playerTwoWins);
            return solution.ToString();
        }

        private Dictionary<int,int> GetDistinctDiceValues()
        {
            Dictionary<int, int> diceValues = new();
            for (int one = 1; one <= 3; one++)
            {
                for (int two = 1; two <= 3; two++)
                {
                    for (int three = 1; three <= 3; three++)
                    {
                        int value = one + two + three;
                        if(diceValues.ContainsKey(value))
                            diceValues[value]++;
                        else
                            diceValues.Add(value, 1);
                    }
                }
            }

            return diceValues;
        }
    }
}