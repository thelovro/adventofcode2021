﻿namespace Days
{
    internal class Day16 : BaseDay
    {
        public Day16()
        {
            SampleInputPartOne.Add(@"D2FE28", "6");
            SampleInputPartOne.Add(@"38006F45291200", "9");
            SampleInputPartOne.Add(@"EE00D40C823060", "14");
            SampleInputPartOne.Add(@"8A004A801A8002F478", "16");
            SampleInputPartOne.Add(@"620080001611562C8802118E34", "12");
            SampleInputPartOne.Add(@"C0015000016115A2E0802F182340", "23");
            SampleInputPartOne.Add(@"A0016C880162017C3686B18A3D4780", "31");

            SampleInputPartTwo.Add(@"C200B40A82", "3");
            SampleInputPartTwo.Add(@"04005AC33890", "54");
            SampleInputPartTwo.Add(@"880086C3E88112", "7");
            SampleInputPartTwo.Add(@"CE00C43D881120", "9");
            SampleInputPartTwo.Add(@"D8005AC2A8F0", "1");
            SampleInputPartTwo.Add(@"F600BC2D8F", "0");
            SampleInputPartTwo.Add(@"9C005AC2F8F0", "0");
            SampleInputPartTwo.Add(@"9C0141080250320F1802104A08", "1");
        }

        protected override string FindFirstSolution(string[] input)
        {
            string binarystring = String.Join(String.Empty,
                input[0].Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            
            (long solution, int versionSum, int length) = ParsePackage(binarystring);

            return versionSum.ToString();
        }

        private (long, int, int) ParsePackage(string binarystring)
        {
            int version = Convert.ToInt32(binarystring.Substring(0, 3), 2);
            int type = Convert.ToInt32(binarystring.Substring(3, 3), 2);
            int versionSum = 0;
            
            long solution;
            int packageLength;
            if (type == 4)
                (solution, packageLength) = ParseLiteral(binarystring.Substring(6));
            else
                (solution, versionSum, packageLength) = ParseOperator(binarystring.Substring(6), type);

            return (solution, versionSum + version, packageLength + 6);
        }

        private (long, int, int) ParseOperator(string binarystring, int type)
        {
            int lengthTypeId = Convert.ToInt32(binarystring.Substring(0, 1));

            long solution;
            int versionSum;
            int packageLength;
            if (lengthTypeId == 0)
                (solution, versionSum, packageLength) = GetValueByLength(binarystring.Substring(1), type);
            else
                (solution, versionSum, packageLength) = GetValueByNumberOfSubpackets(binarystring.Substring(1), type);

            return (solution, versionSum, packageLength + 1);
        }

        private (long, int, int) GetValueByLength(string binarystring, int type)
        {
            int length = Convert.ToInt32(binarystring.Substring(0, 15), 2);
            string packets = binarystring.Substring(15, length);

            long solutionAll = long.MaxValue;
            int versionSum = 0;
            int sumPackageLength = packets.Length;

            while (sumPackageLength > 6)
            {
                (long solution, int version, int packageLength) = ParsePackage(packets);
                sumPackageLength -= packageLength;
                solutionAll = GetValue(solutionAll, solution, type);
                versionSum += version;
                packets = packets.Substring(packageLength);
            }

            return (solutionAll, versionSum, length + 15);
        }

        private (long, int, int) GetValueByNumberOfSubpackets(string binarystring, int type)
        {
            int numberOfPackets = Convert.ToInt32(binarystring.Substring(0, 11), 2);
            string packets = binarystring.Substring(11);

            long solutionAll = long.MaxValue;
            int versionSum = 0;
            int sumPackageLength = packets.Length;
            int usedBits = 0;
            int counter = 0;

            while (counter++ < numberOfPackets)
            {
                (long solution, int version, int packageLength) = ParsePackage(packets);
                sumPackageLength -= packageLength;
                usedBits += packageLength;
                solutionAll = GetValue(solutionAll, solution, type);
                versionSum += version;
                packets = packets.Substring(packageLength);
            }

            return (solutionAll, versionSum, usedBits + 11);
        }

        private long GetValue(long solutionAll, long solution, int type)
        {
            if (solutionAll == long.MaxValue) return solution;

            return type switch
            {
                0 => solutionAll + solution,
                1 => solutionAll * solution,
                2 => Math.Min(solutionAll, solution),
                3 => Math.Max(solutionAll, solution),
                5 => solutionAll > solution ? 1 : 0,
                6 => solutionAll < solution ? 1 : 0,
                7 => solutionAll == solution ? 1 : 0,
                _ => throw new Exception("wtf")
            };
        }

        private (long, int) ParseLiteral(string binarystring)
        {
            string tmp = "";
            int packageLength = 0;
            for (int i = 0; i < binarystring.Length; i += 5)
            {
                tmp += binarystring.Substring(i+1, 4);

                if (binarystring[i].ToString() == "0")
                {
                    packageLength = i + 5;
                    break;
                }
            }

            return (Convert.ToInt64(tmp, 2), packageLength);
        }

        protected override string FindSecondSolution(string[] input)
        {
            string binarystring = String.Join(String.Empty,
                input[0].Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            (long solution, int versionSum, int length) = ParsePackage(binarystring);

            return solution.ToString();
        }
    }
}