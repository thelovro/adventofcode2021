﻿namespace Days;
internal class Day03 : BaseDay
{
    public Day03()
    {
        SampleInputPartOne.Add(@"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010", "198");

        SampleInputPartTwo.Add(@"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010", "230");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int solution = CalculateFirstSolution(input);

        return solution.ToString();
    }

    private int CalculateFirstSolution(string[] input)
    {
        string bitsGamma = "", bitsEpsilon = "";

        for (int i = 0; i < input[0].Length; i++)
        {
            int counterOne = 0, counterZero = 0;

            foreach (var item in input)
            {
                if(item[i]=='0') counterZero++;
                if (item[i] == '1') counterOne++;
            }

            bitsGamma += counterZero > counterOne ? "0" : "1";
            bitsEpsilon += counterZero > counterOne ? "1" : "0";
        }

        int gammaRate = Convert.ToInt32(bitsGamma, 2);
        int epsilonRate = Convert.ToInt32(bitsEpsilon, 2);

        return gammaRate * epsilonRate;
    }

    protected override string FindSecondSolution(string[] input)
    {
        int solution = CalculateSecondSolution(input);

        return solution.ToString();
    }

    private int CalculateSecondSolution(string[] input)
    {
        string bitsOxygen = GetPattern(input, 1);
        string bitsCo2 = GetPattern(input, 0);

        int oxygenRate = Convert.ToInt32(bitsOxygen, 2);
        int co2Rate = Convert.ToInt32(bitsCo2, 2);

        return oxygenRate * co2Rate;
    }

    private static string GetPattern(string[] input, int criteria)
    {
        string pattern = "", bits = "";

        for (int i = 0; i < input[0].Length; i++)
        {
            int counterOne = 0, counterZero = 0;

            foreach (var item in input.Where(x => x.StartsWith(pattern)))
            {
                if (item[i] == '0') counterZero++;
                if (item[i] == '1') counterOne++;
            }

            if (counterOne > counterZero || counterOne == counterZero)
            {
                pattern += criteria == 1 ? "1" : "0";
            }
            else
            {
                pattern += criteria == 1 ? "0" : "1";
            }

            if (input.Where(x => x.StartsWith(pattern)).Count() == 1)
            {
                bits = input.Where(x => x.StartsWith(pattern)).First();
                break;
            }
        }

        return bits;
    }
}