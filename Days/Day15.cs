﻿namespace Days
{
    internal class Day15 : BaseDay
    {
        public Day15()
        {
            SampleInputPartOne.Add(@"1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581", "40");

            SampleInputPartTwo.Add(@"1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581", "315");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long solution = GetSolution(input, 1);

            return solution.ToString();
        }

        private long GetSolution(string[] input, int mapMultiplier)
        {
            Dictionary<(int, int), int> map = GetMap(input);

            if(mapMultiplier > 1)
            {
                map = MultiplyMap(map, mapMultiplier);
            }

            long pathLength = FindLowestRiskPath(map);

            return pathLength;
        }

        private Dictionary<(int, int), int> MultiplyMap(Dictionary<(int, int), int> map, int mapMultiplier)
        {
            Dictionary<(int, int), int> newMap = new();
            int maxX = map.Max(m => m.Key.Item1);
            int maxY = map.Max(m => m.Key.Item2);

            for (int y = 0; y < mapMultiplier; y++)
            {
                for (int x = 0; x < mapMultiplier; x++)
                {
                    foreach(var pair in map)
                    {
                        int value = map[pair.Key] + x + y;
                        if (value > 9) value = value - 9;

                        newMap.Add((x * maxX + pair.Key.Item1 + x, y * maxY + pair.Key.Item2 + y), value);
                    }
                }
            }

            return newMap;
        }

        private long FindLowestRiskPath(Dictionary<(int, int), int> map)
        {
            Queue<((int, int), int)> locations = new();
            locations.Enqueue(((0, 0), map[(0, 0)] * -1));

            Dictionary<(int, int), int> visited = new();
            long minPath = long.MaxValue;

            int maxX = map.Max(m => m.Key.Item1);
            int maxY = map.Max(m => m.Key.Item2);

            while (locations.Count > 0)
            {
                var p = locations.Dequeue();
                int length = p.Item2 + map[p.Item1];
                if (visited.ContainsKey(p.Item1))
                {
                    if (p.Item2 >= visited[p.Item1]) continue;
                    visited[p.Item1] = p.Item2;
                }
                else
                {
                    visited.Add(p.Item1, length);
                }

                if (p.Item1.Item1 == maxX && p.Item1.Item2 == maxY)
                {
                    if (length < minPath)
                        minPath = length;
                }

                if (map.ContainsKey((p.Item1.Item1 - 1, p.Item1.Item2))) locations.Enqueue(((p.Item1.Item1 - 1, p.Item1.Item2), length));
                if (map.ContainsKey((p.Item1.Item1 + 1, p.Item1.Item2))) locations.Enqueue(((p.Item1.Item1 + 1, p.Item1.Item2), length));
                if (map.ContainsKey((p.Item1.Item1, p.Item1.Item2 - 1))) locations.Enqueue(((p.Item1.Item1, p.Item1.Item2 - 1), length));
                if (map.ContainsKey((p.Item1.Item1, p.Item1.Item2 + 1))) locations.Enqueue(((p.Item1.Item1, p.Item1.Item2 + 1), length));
            }

            return minPath;
        }

        private Dictionary<(int, int), int> GetMap(string[] input)
        {
            Dictionary<(int, int), int> map = new();

            for (int y = 0; y < input.Length; y++)
            {
                string key = input[y];
                for (int x = 0; x < key.Length; x++)
                {
                    map.Add((x, y), Convert.ToInt32(key[x].ToString()));
                }
            }

            return map;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = GetSolution(input, 5);

            //not the fastest :( cca 130s
            return solution.ToString();
        }
    }
}