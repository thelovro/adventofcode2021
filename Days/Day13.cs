﻿namespace Days
{
    internal class Day13 : BaseDay
    {
        public Day13()
        {
            SampleInputPartOne.Add(@"6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5", "17");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            (HashSet<(int, int)> dots, List<string> foldInstructions) = ParseInput(input);

            dots = FoldOnce(dots, foldInstructions[0]);

            int solution = dots.Count;

            return solution.ToString();
        }

        private HashSet<(int, int)> FoldOnce(HashSet<(int, int)> dots, string foldInstruction)
        {
            string axis = foldInstruction.Split('=')[0].Replace("fold along ", string.Empty);
            int loc = Convert.ToInt32(foldInstruction.Split('=')[1]);

            if(axis == "x")
            {
                dots = FoldX(dots, loc);
            }
            else
            {
                dots = FoldY(dots, loc);
            }

            return dots;
        }

        private HashSet<(int, int)> FoldX(HashSet<(int, int)> dots, int loc)
        {
            HashSet<(int, int)> foldedDots = new();
            foreach (var dot in dots.Where(d => d.Item1 < loc))
            {
                foldedDots.Add(dot);
            }

            foreach (var dot in dots.Where(d => d.Item1 > loc))
            {
                int x = loc - (dot.Item1 - loc);
                (int, int) newDot = (x, dot.Item2);

                foldedDots.Add(newDot);
            }

            return foldedDots;
        }

        private HashSet<(int, int)> FoldY(HashSet<(int, int)> dots, int loc)
        {
            HashSet<(int, int)> foldedDots = new();
            foreach(var dot in dots.Where(d => d.Item2<loc))
            {
                foldedDots.Add(dot);
            }
            
            foreach (var dot in dots.Where(d => d.Item2 > loc))
            {
                int y = loc - (dot.Item2 - loc);
                (int, int) newDot = (dot.Item1, y);

                foldedDots.Add(newDot);
            }

            return foldedDots;
        }

        private (HashSet<(int, int)>, List<string>) ParseInput(string[] input)
        {
            HashSet<(int, int)> dots = new();
            List<string> foldInstructions = new();

            foreach (string s in input)
            {
                if (s.Contains(','))
                {
                    dots.Add((Convert.ToInt32(s.Split(',')[0]), Convert.ToInt32(s.Split(',')[1])));
                }
                else
                {
                    if (s == string.Empty) continue;
                    foldInstructions.Add(s);
                }
            }

            return (dots, foldInstructions);
        }

        protected override string FindSecondSolution(string[] input)
        {
            (HashSet<(int, int)> dots, List<string> foldInstructions) = ParseInput(input);

            foreach (string foldInstruction in foldInstructions)
            {
                dots = FoldOnce(dots, foldInstruction);
            }

            //write output (dots) to console
            int maxX = dots.Max(d => d.Item1);
            int maxY = dots.Max(d => d.Item2);

            for(int y=0; y<=maxY; y++)
            {
                for (int x = 0; x <= maxX; x++)
                {
                    if(dots.Contains((x,y)))
                        Console.Write("#");
                    else
                        Console.Write(" ");
                }
                Console.WriteLine();
            }

            return "HZKHFEJZ";
        }
    }
}