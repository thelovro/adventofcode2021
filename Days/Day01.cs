﻿namespace Days;
internal class Day01 : BaseDay
{
    public Day01()
    {
        SampleInputPartOne.Add(@"199
200
208
210
200
207
240
269
260
263", "7");


        SampleInputPartTwo.Add(@"199
200
208
210
200
207
240
269
260
263", "5");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int counter = 0;
        int depth = 0;

        foreach(string s in input)
        {
            int currentDepth = Convert.ToInt32(s);

            if (depth == 0)
                depth = currentDepth;

            if (depth < currentDepth)
                counter++;

            depth = currentDepth;
        }

        return counter.ToString();
    }

    protected override string FindSecondSolution(string[] input)
    {
        int counter = 0;
        int depth = 0;

        for(int i=2; i<input.Length; i++)
        {
            int currentDepth = Convert.ToInt32(input[i-2])+ Convert.ToInt32(input[i-1])+ Convert.ToInt32(input[i]);

            if (depth == 0)
                depth =currentDepth;

            if (depth < currentDepth)
                counter++;

            depth = currentDepth;
        }

        return counter.ToString();
    }
}