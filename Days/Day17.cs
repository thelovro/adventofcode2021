﻿namespace Days
{
    internal class Day17 : BaseDay
    {
        public Day17()
        {
            SampleInputPartOne.Add(@"target area: x=20..30, y=-10..-5", "45");

            SampleInputPartTwo.Add(@"target area: x=20..30, y=-10..-5", "112");
        }

        protected override string FindFirstSolution(string[] input)
        {
            (int targetXMin, int targetXMax, int targetYMin, int targetYMax) = ParseInput(input[0]);

            List<int> hits = StartShooting(targetXMin, targetXMax, targetYMin, targetYMax);

            return hits.Max().ToString();
        }

        private List<int> StartShooting(int targetXMin, int targetXMax, int targetYMin, int targetYMax)
        {
            List<int> hits = new();
            for(int initSpeedX = 0; initSpeedX < 500; initSpeedX++)
            {
                for (int initSpeedY = -200; initSpeedY < 200; initSpeedY++)
                {
                    (int, int) position = (0, 0);
                    int velocityX = initSpeedX;
                    int velocityY = initSpeedY;
                    int maxYPosition = 0;
                    while ((position.Item1 <= targetXMax && velocityX > 0 ||
                            position.Item1 >= targetXMin && position.Item1 <= targetXMax && velocityX == 0)
                        && position.Item2 >= targetYMin)
                    {
                        position.Item1 += velocityX;
                        position.Item2 += velocityY;
                        if (position.Item2 > maxYPosition)
                            maxYPosition = position.Item2;

                        if (velocityX != 0)
                            velocityX = velocityX > 0 ? velocityX - 1 : velocityX + 1;

                        velocityY--;

                        if (position.Item1 >= targetXMin &&
                            position.Item1 <= targetXMax &&
                            position.Item2 >= targetYMin &&
                            position.Item2 <= targetYMax)
                        {
                            hits.Add(maxYPosition);
                            break;
                        }
                    }
                }
            }

            return hits;
        }

        private (int targetXMin, int targetYMin, int targetXMax, int targetYMax) ParseInput(string input)
        {
            int targetXMin, targetYMin, targetXMax, targetYMax;
            string text = input.Replace("target area: ","");
            targetXMin = Convert.ToInt32(text.Split(", ")[0].Substring(2).Split("..")[0]);
            targetXMax = Convert.ToInt32(text.Split(", ")[0].Substring(2).Split("..")[1]);
            targetYMin = Convert.ToInt32(text.Split(", ")[1].Substring(2).Split("..")[0]);
            targetYMax = Convert.ToInt32(text.Split(", ")[1].Substring(2).Split("..")[1]);

            return (targetXMin, targetXMax, targetYMin, targetYMax);
        }

        protected override string FindSecondSolution(string[] input)
        {
            (int targetXMin, int targetXMax, int targetYMin, int targetYMax) = ParseInput(input[0]);

            List<int> hits = StartShooting(targetXMin, targetXMax, targetYMin, targetYMax);

            return hits.Count().ToString(); 
        }
    }
}