﻿namespace Days
{
    internal class Day12 : BaseDay
    {
        public Day12()
        {
            SampleInputPartOne.Add(@"start-A
start-b
A-c
A-b
b-d
A-end
b-end", "10");

            SampleInputPartOne.Add(@"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc", "19");

            SampleInputPartOne.Add(@"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW", "226");

            SampleInputPartTwo.Add(@"start-A
start-b
A-c
A-b
b-d
A-end
b-end", "36");

            SampleInputPartTwo.Add(@"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc", "103");

            SampleInputPartTwo.Add(@"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW", "3509");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<(string, string)> cavesConnections = GetCavesConnections(input);

            List<string> paths = FindPaths(cavesConnections, "start", false);

            return paths.Count.ToString();
        }

        private List<string> FindPaths(List<(string, string)> cavesConnections, string path, bool canVisitSmallCaveTwice)
        {
            string lastItem = path.Split(',')[path.Split(',').Length - 1];
            List<string> paths = new();

            foreach ((string, string) connection in cavesConnections.Where(c => c.Item1 == lastItem || c.Item2 == lastItem))
            {
                string newItem = (connection.Item1 == lastItem ? connection.Item2 : connection.Item1);
                if (path.Contains(newItem)
                    && newItem == newItem.ToLower() 
                    && (!canVisitSmallCaveTwice || (newItem.Length > 2 || DidIVisitSmallCaveTwice(path))))
                    continue;

                string newPath = $"{path},{newItem}";
                if (newItem == "end")
                    paths.Add(newPath);
                else
                    paths.AddRange(FindPaths(cavesConnections, newPath, canVisitSmallCaveTwice));
            }

            return paths;
        }

        private List<(string, string)> GetCavesConnections(string[] input)
        {
            List<(string, string)> caves = new();
            foreach (string row in input)
            {
                caves.Add((row.Split('-')[0], row.Split('-')[1]));
            }

            return caves;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<(string, string)> cavesConnections = GetCavesConnections(input);

            List<string> paths = FindPaths(cavesConnections, "start", true);

            return paths.Count.ToString();
        }

        private bool DidIVisitSmallCaveTwice(string path)
        {
            return path.Split(',')
                .Where(p => p == p.ToLower())
                .GroupBy(p => p)
                .Select(g => new { g.Key, Count = g.Count() })
                .Any(g => g.Count > 1);
        }
    }
}