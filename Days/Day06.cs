﻿namespace Days
{
    internal class Day06 : BaseDay
    {
        public Day06()
        {
            SampleInputPartOne.Add(@"3,4,3,1,2", "5934");

            SampleInputPartTwo.Add(@"3,4,3,1,2", "26984457539");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long solution = CountFish(input, 80);

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            long solution = CountFish(input, 256);

            return solution.ToString();
        }

        private long CountFish(string[] input, int dayLimit)
        {
            //parse input
            Dictionary<int, long> fishes = GetNewDictionary();
            foreach (string s in input[0].Split(','))
            {
                fishes[Convert.ToInt32(s)]++;
            }
            
            for (int i=0; i<dayLimit; i++)
            {
                Dictionary<int, long> newDayFishes = GetNewDictionary();
                for (int d = 1; d <= 8; d++)
                {
                    newDayFishes[d - 1] = fishes[d];
                }
                newDayFishes[6] += fishes[0];
                newDayFishes[8] += fishes[0];

                fishes = newDayFishes;
            }

            return fishes.Sum(x => x.Value);
        }

        private Dictionary<int, long> GetNewDictionary()
        {
            Dictionary<int, long> fishes = new();
            for (int i = 0; i <= 8; i++)
            {
                fishes.Add(i, 0);
            }

            return fishes;
        }
    }
}