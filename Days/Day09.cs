﻿namespace Days
{
    internal class Day09 : BaseDay
    {
        public Day09()
        {
            SampleInputPartOne.Add(@"2199943210
3987894921
9856789892
8767896789
9899965678", "15");

            SampleInputPartTwo.Add(@"2199943210
3987894921
9856789892
8767896789
9899965678", "1134");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int, int), int> map = GetMap(input);
            List<KeyValuePair<(int, int), int>> points = GetLowestPoints(map);

            int solution = points.Select(p => p.Value).Sum() + points.Count;
            return solution.ToString();
        }

        private static List<KeyValuePair<(int, int), int>> GetLowestPoints(Dictionary<(int, int), int> map)
        {
            List<KeyValuePair<(int, int), int>> points = new();
            foreach (var point in map)
            {
                if (map.ContainsKey((point.Key.Item1 - 1, point.Key.Item2)) && map[(point.Key.Item1 - 1, point.Key.Item2)] <= point.Value
                    || map.ContainsKey((point.Key.Item1 + 1, point.Key.Item2)) && map[(point.Key.Item1 + 1, point.Key.Item2)] <= point.Value
                    || map.ContainsKey((point.Key.Item1, point.Key.Item2 - 1)) && map[(point.Key.Item1, point.Key.Item2 - 1)] <= point.Value
                    || map.ContainsKey((point.Key.Item1, point.Key.Item2 + 1)) && map[(point.Key.Item1, point.Key.Item2 + 1)] <= point.Value)
                    continue;

                points.Add(point);
            }

            return points;
        }

        private static Dictionary<(int, int), int> GetMap(string[] input)
        {
            Dictionary<(int, int), int> map = new();
            for (int y = 0; y < input.Length; y++)
            {
                string s = input[y];
                for (int x = 0; x < s.Length; x++)
                {
                    map.Add((x, y), Convert.ToInt32(s[x].ToString()));
                }
            }

            return map;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(int, int), int> map = GetMap(input);

            List<KeyValuePair<(int, int), int>> points = GetLowestPoints(map);
            Dictionary<(int, int), int> basins = GetBasins(points, map);

            var x = basins.OrderByDescending(b => b.Value).Take(3).Select(b => b.Value).ToList();
            int solution = x[0] * x[1] * x[2];

            return solution.ToString();
        }

        private Dictionary<(int, int), int> GetBasins(List<KeyValuePair<(int, int), int>> points, Dictionary<(int, int), int> map)
        {
            Dictionary<(int, int), int> basins = new();
            foreach (var point in points)
            {
                int size = GetBasinSize(point, map);
                basins.Add(point.Key, size);
            }

            return basins;
        }

        private int GetBasinSize(KeyValuePair<(int, int), int> point, Dictionary<(int, int), int> map)
        {
            int size = 0;
            HashSet<(int, int)> visited = new();
            Queue<(int, int)> queue = new();
            queue.Enqueue(point.Key);

            while (queue.Count > 0)
            {
                var p = queue.Dequeue();
                if (visited.Contains(p)) continue;

                visited.Add(p);
                size++;

                if (map.ContainsKey((p.Item1 - 1, p.Item2)) && map[(p.Item1 - 1, p.Item2)] < 9) queue.Enqueue((p.Item1 - 1, p.Item2));
                if (map.ContainsKey((p.Item1 + 1, p.Item2)) && map[(p.Item1 + 1, p.Item2)] < 9) queue.Enqueue((p.Item1 + 1, p.Item2));
                if (map.ContainsKey((p.Item1, p.Item2 - 1)) && map[(p.Item1, p.Item2 - 1)] < 9) queue.Enqueue((p.Item1, p.Item2 - 1));
                if (map.ContainsKey((p.Item1, p.Item2 + 1)) && map[(p.Item1, p.Item2 + 1)] < 9) queue.Enqueue((p.Item1, p.Item2 + 1));
            }

            return size;
        }
    }
}